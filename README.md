DBMyadmin
=========

A Web interface for mysql which aims to run at mobile devices, 
sub set feature w.r.t. phpmyadmin.

- Can run on mobile devices
- History of Queries are saved
- Dropdown has queries
- Keybiding added (Press Ctrl+Enter to Process Query)

Inspired from lack of features in phpmyadmin!

##Installation
Download the [latest release](https://github.com/peeyushsrj/dbmyadmin/releases) and upload the folder to the httpdocs directory (e.g. /var/www/html) of your server.