<form action='index.php' method='POST'>
<div class="dropdown" data-toggle="tooltip" title="Press Ctrl+WinKey" data-placement="left" >
  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" >SQL Query
  <span class="caret"></span></button>
  <ul class="dropdown-menu" >
    <li><a href="#" command="SHOW TABLES">SHOW</a></li>
    <li><a href="#" command="DESCRIBE $tbl">DESCRIBE</a></li>
    <li><a href="#" command="SELECT $col FROM $tbl WHERE $cond">SELECT</a></li>
    <li><a href="#" command="INSERT INTO $tbl VALUES()">INSERT</a></li>
    <li><a href="#" command="UPDATE $tbl SET $col=$val WHERE">UPDATE</a></li>
    <li><a href="#" command="DELETE FROM $tbl WHERE $cond">DELETE</a></li>
    <li><a href="#" command="ALTER TABLE $tbl">ALTER</a></li>
  </ul>
<button class="btn btn-default" data-toggle="tooltip" title="Press Ctrl+Alt+C" data-placement="right" onclick="taReset()">CLEAR</button>
</div>
<?php
echo "<textarea placeholder='Type in SQL...' name='sqlcom' id='ta'>" . $_SESSION['sqlcom'] . "</textarea>";
echo "<input type='hidden' name='host' value='" . $_SESSION['host'] . "' />";
echo "<input type='hidden' name='usrname' value='" . $_SESSION['usrname'] . "' />";
echo "<input type='hidden' name='psw' value='" . $_SESSION['psw'] . "' />";
echo "<input type='hidden' name='dbname' value='" . $_SESSION['dbname'] . "' />";
?>
	<input type='hidden' name='sqlkey' value='1' />
	<input type='hidden' name='dbkey' value='1' />
	<input type='submit' class='btn btn-success' value='PROCESS' data-toggle="tooltip" title="Press Ctrl+Enter" data-placement="right" /><br><br>
</form>
<script src="lib/jquery.autogrow.js"></script>
<script>
$(function() {
	pageCounter(); console.log(getPageCount());
	$('form textarea').autogrow();
	if (getPageCount()<1) {
		$('[data-toggle="tooltip"]').tooltip();
	}
})
</script>