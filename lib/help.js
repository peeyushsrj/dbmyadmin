function taClear() {
    $("#ta").val("");
}

function taFocus() {
    $("#ta").focus();
}

function taReset() {
    taClear()
    taFocus()
}

function taGetValue() {
    return $("#ta").val()
}

function taSetValue(value) {
    $("#ta").val(value)
}

function addToCookie(val) {
    var s = "cv=" + val
    document.cookie = s
}

function pageRefresh() {
    location.href = "/"
}

function pageCounter() {
    if (sessionStorage.count) {
        sessionStorage.count = Number(sessionStorage.count) + 1
    } else {
        sessionStorage.count = 0
    }
}

function getPageCount() {
    return sessionStorage.count
}

function setPageCount(n) {
    sessionStorage.count = n
}

function panelToggle() {
    if ($('#panel').is(':hidden')) {
        $('.table').html("")
        $('#panel').html("Loading...")
        $.post("history.php", { cmd: "John" }, function(data) {
            $("#panel").html(data)
        })
    }
    $('#panel').slideToggle()
}

function logout() {
    sessionStorage.count = 0
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'logout.php', true);
    xhr.send();
    location.href = "/"
}
