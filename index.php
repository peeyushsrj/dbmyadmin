<?php session_start();
$_SESSION["usrname"] = $_POST["usrname"];
$_SESSION["psw"] = $_POST["psw"];
$_SESSION["host"] = $_SESSION["host"];
$_SESSION["dbname"] = $_POST["dbname"];
$_SESSION["sqlcom"] = $_POST["sqlcom"];
$_SESSION["cmd"];
?>
<!DOCTYPE html>
<html>
<head>
	<title>DBMyadmin</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="lib/bootstrap.min.css">
	<script src="lib/jquery.min.js"></script>
	<script src="lib/bootstrap.min.js"></script>
	<script src="lib/help.js"></script>
	<style type="text/css">
	body{background-color:#eee;background-image:url(ignasi_pattern_s.png)} .main{margin-top:9vh}
	textarea{width:100%;border:1px solid #eee;outline:0;min-height:12vh;padding:10px 15px;font-family:monospace;color:#3d3d3d;margin-top:8px;margin-bottom:5px} textarea:focus{border:1px solid #ccc} .table{background: #fff;}
	</style>
	<script>
		$(document).ready(function(){
			taFocus();panelToggle();
			$('.dropdown li').on('click', function(){
				var command=$(this).children().attr("command");
				taSetValue(command);addToCookie(command);taFocus();
			});
			//Disable notification after reload
			$("#notf").delay(2000).fadeOut(1000);
			$("#panel").hide();
			$('#ta').keydown(function (e) {
  				if (e.ctrlKey && e.keyCode == 13) {
  					this.form.submit();
  				}
  				if (e.ctrlKey && e.shiftKey && e.keyCode == 67) {
  					taClear();e.preventDefault();
  				}
  				if (e.ctrlKey && e.keyCode == 91) {
  					$(".dropdown-menu").dropdown("toggle");
  				}
			})
		})
	</script>
</head>
<body class="container">
<div class="text-right">
<?php
if ($_POST['usrname']) {
	echo '<a href="#" onclick="panelToggle()">History</a>&nbsp;<a href="#" onclick="logout()">Logout</a>';
}
?>
</div>
<div class="main col-md-offset-1 col-md-10">
<?php
if (isset($_POST["dbkey"])) {
	$db = new mysqli($_SESSION['host'], $_SESSION['usrname'], $_SESSION['psw'], $_SESSION['dbname']);
	if ($db->connect_errno > 0) {
		die("<p class='text-danger text-center bg-danger'>Unable to conenct db: " . $db->connect_error . ", <a href='man.php'>Check manual</a></p>");
	}
	include 'form2.php';
	$sql = $_POST["sqlcom"];
	$hishyperlink = '<a href="#" onclick="taSetValue(' . $sql . '); taFocus();">' . $sql . '</a>';
	$_SESSION["cmd"] = $hishyperlink . "<br>" . $_SESSION["cmd"];
	echo "<div class='well' id='panel'></div>";
	$result = $db->query($sql);
	if (!$result) {die("<p class='text-danger text-center bg-danger'>" . $db->error . "</p>");}
	include 'func.php';
} else {include 'form1.html';}
?>
</div>
</body>
</html>