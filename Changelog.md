# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

UI Improvement, Addition of themes, Fast Resource Loading.
### Added
- Keybinding to Process SQL (Ctrl+Enter)
- Auto resizing text field

## [0.1.1] - 2016-18-02

UI Tweaked (boostrap classes changed), SQL Dropdown improved.
### Changed
- Autocomplete in removed, default values are used.